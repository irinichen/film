var Index 		= require('../app/controllers/index')
var User 		= require('../app/controllers/user')
var Movie 		= require('../app/controllers/movie')
var Category 	= require('../app/controllers/category')
var Type 		= require('../app/controllers/type')
var Director 	= require('../app/controllers/director')
var Article 	= require('../app/controllers/article')
var Slide 		= require('../app/controllers/slide')
var Message 	= require('../app/controllers/message')
var Search 		= require('../app/controllers/search')
var Api 		= require('../app/controllers/api');
var _ 			= require('underscore')

module.exports = function(app) {
	// PRE HANDLE USER
	app.use(function(req, res, next) {
		var _user = req.session.user

		app.locals.user = _user
		
		next()	
	})

	// APIs ====================================================
	// Category
	app.get('/api/categories', Api.categories);
	app.get('/api/categories/:id', Api.category);
	app.delete('/api/categories/:id', Api.cateDelete);

	// Type
	app.get('/api/types', Api.types);
	app.get('/api/types/:id', Api.type);
	app.delete('/api/types/:id', Api.typeDelete);


	// PAGES ===================================================
	app.get('/', Index.index)
	app.get('/about', Index.about)
	app.get('/translation', Index.translation)
	app.get('/admin', User.signin)
	app.post('/mailsend', Message.send)

	app.get('/en', Index.indexEn)
	app.get('/en/about', Index.aboutEn)
	app.get('/en/translation', Index.translationEn)
	app.get('/en/:page', Index.pageEn)
	app.get('/en/search/:category/:director', Search.cndEn)
	app.get('/en/find/:type', Search.typeEn)
	app.get('/en/movie/:id', Movie.detailEn)

	app.get('/search/:category/:director', Search.cnd)
	app.get('/find/:type', Search.type)
	app.get('/:page', Index.page)
	

	// app.get('/documentary', Index.documentary)
	// app.get('/animation', Index.animation)
	// app.get('/tv', Index.tv)
	// app.get('/short', Index.short)
	app.get('/all', Index.all)
	app.get('/article/:id', Article.detail)

	app.get('/category', Category.show)
	app.get('/:director', Index.movies)

	// USER
	
	app.get('/admin/user', User.signinRequired, User.adminRequired, User.list)
	app.get('/admin/user/add', User.signinRequired, User.adminRequired, User.add)
	app.post('/admin/user/new', User.signinRequired, User.adminRequired, User.signup)
	app.post('/admin/login', User.login)
	app.get('/admin/logout', User.logout)

	// MOVIE
	app.get('/movie/:id', Movie.detail)

	//	ADMIN PANEL MOVIE
	app.get('/admin/movie', User.signinRequired, User.adminRequired, Movie.list)
	app.get('/admin/movie/add', User.signinRequired, User.adminRequired, Movie.new)
	app.get('/admin/movie/update/:id', User.signinRequired, User.adminRequired, Movie.update)
	app.post('/admin/movie/new', User.signinRequired, User.adminRequired, Movie.savePoster, Movie.save)
	app.post('/admin/movie', User.signinRequired, User.adminRequired, Movie.del)
	app.get('/admin/movie/video/:id', User.signinRequired, User.adminRequired, Movie.uploadVideo)
	app.post('/admin/movie/video/save', User.signinRequired, User.adminRequired, Movie.saveVideo, Movie.updateByVideo)
	app.get('/admin/movie/load/type/:category', User.signinRequired, User.adminRequired, Movie.loadType)
	app.get('/admin/movie/gallery/:id', User.signinRequired, User.adminRequired, Movie.uploadPhoto)
	app.post('/admin/movie/gallery/save', User.signinRequired, User.adminRequired, Movie.savePhoto)
	app.post('/admin/movie/gallery', User.signinRequired, User.adminRequired, Movie.delPhoto)
	app.get('/admin/movie/:page', User.signinRequired, User.adminRequired, Movie.search)

	//	ADMIN PANEL CATEGORY
	app.get('/admin/category/add', User.signinRequired, User.adminRequired, Category.add)
	app.get('/admin/category', User.signinRequired, User.adminRequired, Category.list)
	app.post('/admin/category/save', User.signinRequired, User.adminRequired, Category.save)
	app.post('/admin/category', User.signinRequired, User.adminRequired, Category.del)

	//	ADMIN PANEL TYPE
	app.get('/admin/type/add', User.signinRequired, User.adminRequired, Type.add)
	app.get('/admin/type', User.signinRequired, User.adminRequired, Type.list)
	app.post('/admin/type/save', User.signinRequired, User.adminRequired, Type.save)
	app.post('/admin/type', User.signinRequired, User.adminRequired, Type.del)

	//	ADMIN PANEL DIRECTOR
	app.get('/admin/director/add', User.signinRequired, User.adminRequired, Director.add)
	app.get('/admin/director', User.signinRequired, User.adminRequired, Director.list)
	app.get('/admin/director/update/:id', User.signinRequired, User.adminRequired, Director.update)
	app.post('/admin/director/save', User.signinRequired, User.adminRequired, Director.save)
	app.post('/admin/director', User.signinRequired, User.adminRequired, Director.del)

	// ADMIN PANEL ARTICLE
	app.get('/admin/article', User.signinRequired, User.adminRequired, Article.list)
	app.get('/admin/article/add', User.signinRequired, User.adminRequired, Article.add)
	// app.get('/admin/article/update/:id', User.signinRequired, User.adminRequired, Article.update)
	app.post('/admin/article/save', User.signinRequired, User.adminRequired, Article.save)
	app.post('/admin/article', User.signinRequired, User.adminRequired, Article.del)

	// ADMIN PANEL SLIDE
	app.get('/admin/slide', User.signinRequired, User.adminRequired, Slide.list)
	app.get('/admin/slide/add', User.signinRequired, User.adminRequired, Slide.add)
	app.get('/admin/slide/update/:id', User.signinRequired, User.adminRequired, Slide.update)
	app.post('/admin/slide/save', User.signinRequired, User.adminRequired, Slide.savePictures, Slide.save)
	app.post('/admin/slide', User.signinRequired, User.adminRequired, Slide.del)

}