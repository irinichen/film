var express 		= require('express')
var path 			= require('path')
var mongoose 		= require('mongoose')
var mongoStore 		= require('connect-mongodb')
var morgan 			= require('morgan')
var multipart 		= require('connect-multiparty')
var methodOverride 	= require('method-override')

var port 			= process.env.PORT || 3000
var app 			= express()		// 启动web服务器
var cookieParser 	= require('cookie-parser')
var bodyParser 		= require('body-parser')
var session 		= require('express-session')
var dbUrl 			= 'mongodb://localhost/demo'

mongoose.connect(dbUrl)

app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/vnd.api+json' }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(multipart())

app.set('views', './app/views/pages')
app.set('view engine', 'jade')


app.use(cookieParser())		// 引入这个后session才能正常工作
app.use(session({
	secret: 'film',
	store: new mongoStore({
		url: dbUrl,
		collection: 'session'
	})
}))

if ('development' === app.get('env')) {
	app.set('showStackError', true)
	app.use(morgan(':method :url :status'))
	app.locals.pretty = true
	mongoose.set('debug', true)
}

require('./config/routes')(app)

app.listen(port)
app.locals.moment = require('moment')
app.use(express.static(path.join(__dirname, 'public')))

console.log('Magic On!! on ' + port)
