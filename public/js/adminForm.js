require.config({
	baseUrl: '../../js/',
	paths: {
		avalon: 'tools/avalon',
		domReady: 'tools/domReady'
	},
	shim: {
		avalon: { exports: 'avalon' }
	}
});

require(['avalon', "domReady!"], function() {

	// 分类信息 =================================================
	var category_m = avalon.define('category', function(vm) {
		vm.categories = {};

		// BEGIN OF REMOVE
		vm.remove = function(el) {

			var cid = el._id;

			$.ajax({
				type: 'delete',
				url: '/api/categories/' + cid
			})
			.done(function(result) {
				if (result.msg == 'success') {
					vm.categories.remove(el);
					alert('删除成功！');
				} else {
					alert(result.msg);
				};
			});

		};
		// END OF REMOVE
	});

	$(function() {
		$.getJSON('/api/categories').done(function(data) {
			category_m.categories = data;
		});
	});

	avalon.scan();
});