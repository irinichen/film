$(document).ready(function(){
	// 全部及按导演筛选切换
	$('.getall').click(function() {
		$('.byDirector').parent('.col-md-1').removeClass('active');
		$('#allBtn').addClass('active');
		$('#allList').fadeIn();
		$('#filmList1').empty();
	});
	$('.byDirector').click(function(){
		var category = $('#page_slug').attr('value');
		var director = $(this).attr('value');
		$('.byDirector').parent('.col-md-1').removeClass('active');
		$('#allBtn').removeClass('active');
		$(this).parent('.col-md-1').addClass('active');
		$('#allList').fadeOut();
		$('#filmList1').fadeOut();
		var url = './search/' + category + '/' + director;
		$('#filmList1').load(url).fadeIn();
	});

	// 推荐及按类型筛选切换
	$('.recommend').click(function(){
		$('.byType').parent('.col-md-2').removeClass('active');
		$('#reBtn').addClass('active');
		$('#recommendList').fadeIn();
		$('#filmList2').empty();
	});
	$('.byType').click(function(){
		var type = $(this).attr('value');
		$('.byType').parent('.col-md-2').removeClass('active');
		$('#reBtn').removeClass('active');
		$(this).parent('.col-md-2').addClass('active');
		$('#recommendList').fadeOut();
		$('#filmList2').fadeOut();
		var url = './find/' + type;
		$('#filmList2').load(url).fadeIn();
	});
});