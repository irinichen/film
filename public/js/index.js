$(document).ready(function(){
	var wholeWidth = $('body').width();
	// 最新译制的切换
	var len = $('#filmBlockCarousel .row .col-md-3').length;
	if (len <= 4) {
		$('.more-right').attr('disabled', 'disabled');
	};
	var move = 340;
	$('.more-right').click(function(){
		$(this).attr('disabled', 'disabled');
		$('.more-left').removeAttr('disabled');
		$('#filmBlockCarousel .row').animate({'margin-top': -move}, 1000);
	});
	$('.more-left').click(function(){
		$(this).attr('disabled', 'disabled');
		$('.more-right').removeAttr('disabled');
		$('#filmBlockCarousel .row').animate({'margin-top': 0}, 1000);
	});
	if (wholeWidth < 990) {
		$('#filmBlockCarousel .wrapper').css({'height':'auto', 'overflow':'visible'});
		$('#moreBtn').hide();
	} else {
		$('#filmBlockCarousel .wrapper').css({'height':330, 'overflow':'hidden'});
		$('#moreBtn').show();
	}
	$(window).resize(function(){
		if (wholeWidth < 990) {
			$('#filmBlockCarousel .wrapper').css({'height':'auto', 'overflow':'visible'});
			$('#moreBtn').hide();
		} else {
			$('#filmBlockCarousel .wrapper').css({'height':330, 'overflow':'hidden'});
			$('#filmBlockCarousel .wrapper').css('overflow', 'hidden');
			$('#moreBtn').show();
		};
	});

	// WOW初始化
	var wow = new WOW({
		mobile: false
	});
	wow.init();
	

	// 快捷联络
	$('#contactForm').Validform({
		ajaxPost : true,
			tiptype:function(msg,o,cssctl){
				var objtip=o.obj.siblings(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.html('<div class="tooltip-inner">'+msg+'</div><div class="tooltip-arrow"></div>');
			},
			callback : function(status){
				$('#ok').show();
				$('#contactForm').hide();
			}
	});
});