$(document).ready(function(){
	// 导航的响应式变化
	$container = $( "body" );
	if ($container.width() < 992) {
		$('nav.navbar').removeClass('navbar-fixed-top');
	} else if ($('nav.navbar').hasClass('navbar-fixed-top').toString() == 'false') {
		$('nav.navbar').addClass('navbar-fixed-top');
	};
	$(window).resize(function(){
		if ($container.width() < 992) {
			$('nav.navbar').removeClass('navbar-fixed-top');
		} else if ($('nav.navbar').hasClass('navbar-fixed-top').toString() === 'false') {
			$('nav.navbar').addClass('navbar-fixed-top');
		};
	});

	// 根据窗口大小设置slide图片高度
	var wid = $(window).width();
	var hei = wid / 2;
	$('#myCarousel').css('height', hei);
	$('#myCarousel .item').css('height', hei);
	$('#myCarousel .carousel-inner > .item > img').css('height', hei);

	$(window).resize(function(){
		var wid = $(window).width();
		var hei = wid / 2;
		$('#myCarousel').css('height', hei);
		$('#myCarousel .item').css('height', hei);
		$('#myCarousel .carousel-inner > .item > img').css('height', hei);
	});

	// 判断carousel-indicators的个数
	var n = $('.carousel-inner .bigitem').length;
	for (var i = 0; i < n; i++) {
		var html = '<li data-slide-to="' + i + '" data-target="#myCarousel"></li>';
		$('ol.carousel-indicators').append(html);
	};

	// 设置slide默认当前项
	$('.carousel-inner .bigitem:eq(0)').addClass('active');
	$('.carousel-indicators li:eq(0)').addClass('active');
	if (n > 1) {
		$('#myCarousel').hover(
			function(){
				$('a.carousel-control').fadeIn();
			},
			function(){
				$('a.carousel-control').fadeOut();
			}
		);
	}
});