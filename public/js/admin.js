$(document).ready(function(){
    $('#cateBox option').click(function(){
        var category = $(this).attr('value');
        var url = './load/type/' + category;
        $('#typeContent').load(url).fadeIn();
    });
    $('.del').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/movie?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    $('.delC').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/category?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    $('.delT').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/type?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    $('.delD').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/director?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    $('.delA').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/article?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    $('.delS').click(function (e) {
        var target = $(e.target);
        var id = target.data('id');
        var tr = $('.item-id-'+id);
        $.ajax({
            type: 'POST',
            url: "/admin/slide?id=" + id
        })
        .done(function (results) {
            if (results.success === 1) {
                if (tr.length > 0) {
                    tr.remove()
                }
            }
        })
    })

    // Delete photo in a movie
    $('.delPhoto').click(function(e) {
        var target = $(e.target);
        var id = target.data('id');
        var name = target.data('name');
        $(this).parent('.col-md-3').remove();
        $.ajax({
            type: 'POST',
            url: '/admin/movie/gallery?id=' + id + '&name=' + name
        })
        .done(function(results) {
            if (results.success === 1) {
                location.reload();
            }
        });
    });

    // The navigantion of MOVIES
    $('a.goMovie').click(function(e) {
        var target = $(e.target);
        var page = target.data('page');
        var url = '/admin/movie/' + page;
        // alert(url);
        $('#listTable').empty();
        $('#listTable').css('opacity', 0);
        $('#listTable').load(url, function() {
            $(this).animate({opacity:'1'},"slow");
        });
    });

})