$(document).ready(function(){
  // 导航的响应式变化
  $container = $( "body" );
  if ($container.width() < 992) {
    $('nav.navbar').removeClass('navbar-fixed-top');
  } else if ($('nav.navbar').hasClass('navbar-fixed-top').toString() == 'false') {
    $('nav.navbar').addClass('navbar-fixed-top');
  };
  $(window).resize(function(){
    if ($container.width() < 992) {
      $('nav.navbar').removeClass('navbar-fixed-top');
    } else if ($('nav.navbar').hasClass('navbar-fixed-top').toString() === 'false') {
      $('nav.navbar').addClass('navbar-fixed-top');
    };
  });

  // 根据窗口大小设置slide图片高度
  var wid = $(window).width();
  var hei = wid / 2;
  $('#myCarousel').css('height', hei);
  $('#myCarousel .item').css('height', hei);
  $('#myCarousel .carousel-inner > .item > img').css('height', hei);

  $(window).resize(function(){
    var wid = $(window).width();
    var hei = wid / 2;
    $('#myCarousel').css('height', hei);
    $('#myCarousel .item').css('height', hei);
    $('#myCarousel .carousel-inner > .item > img').css('height', hei);
  });

  // 判断carousel-indicators的个数
  var n = $('.carousel-inner .bigitem').length;
  for (var i = 0; i < n; i++) {
    var html = '<li data-slide-to="' + i + '" data-target="#myCarousel"></li>';
    $('ol.carousel-indicators').append(html);
  };

  // 设置slide默认当前项
  $('.carousel-inner .bigitem:eq(0)').addClass('active');
  $('.carousel-indicators li:eq(0)').addClass('active');
  if (n > 1) {
    $('#myCarousel').hover(
      function(){
        $('a.carousel-control').fadeIn();
      },
      function(){
        $('a.carousel-control').fadeOut();
      }
    );
  }

  // 最新译制的切换
  var len = $('#filmBlockCarousel .row .col-md-3').length;
  if (len <= 4) {
    $('.more-right').attr('disabled', 'disabled');
  };
  var move = 1000;
  $('.more-right').click(function(){
    $(this).attr('disabled', 'disabled');
    $('.more-left').removeAttr('disabled');
    $('#filmBlockCarousel .row').animate({'margin-left': -move}, 1000);
  });
  $('.more-left').click(function(){
    $(this).attr('disabled', 'disabled');
    $('.more-right').removeAttr('disabled');
    $('#filmBlockCarousel .row').animate({'margin-left': 0}, 1000);
  });

  // 全部及按导演筛选切换
  $('.getall').click(function() {
    $('#allList').fadeIn();
    $('#filmList1').empty();
  });
  $('.byDirector').click(function(){
    var category = $('#page_slug').attr('value');
    var director = $(this).attr('value');
    $('.byDirector').parent('.col-md-1').removeClass('active');
    $('#allBtn').removeClass('active');
    $(this).parent('.col-md-1').addClass('active');
    $('#allList').empty();
    $('#filmList1').fadeOut();
    var url = './search/' + category + '/' + director;
    $('#filmList1').load(url).fadeIn();
  });

  // 推荐及按类型筛选切换
  $('.recommend').click(function(){
    $('#recommendList').fadeIn();
    $('#filmList2').empty();
  });
  $('.byType').click(function(){
    var type = $(this).attr('value');
    $('#recommendList').fadeOut();
    $('#filmList2').fadeOut();
    var url = './find/' + type;
    $('#filmList2').load(url).fadeIn();
  });

  // 快捷联络
  $('#contactForm').Validform({
    ajaxPost : true,
      tiptype:function(msg,o,cssctl){
            var objtip=o.obj.siblings(".Validform_checktip");
            cssctl(objtip,o.type);
            objtip.html('<div class="tooltip-inner">'+msg+'</div><div class="tooltip-arrow"></div>');
            objtip.show();
        },
        callback : function(status){
            $('#ok').show();
            $('#contactForm').hide();
        }
  });
});