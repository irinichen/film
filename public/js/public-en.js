$(document).ready(function(){
  // 根据窗口大小设置slide图片高度
  var wid = $(window).width();
  if (wid < 1190) {
    $('.navbar-default').removeClass('navbar-fixed-top');
    $('#menu').css('margin-left', 0);
  } else if (wid > 1190) {
    $('#menu').css('margin-left', 94);
  }
  var hei = wid / 2;
  $('#myCarousel').css('height', hei);
  $('#myCarousel .item').css('height', hei);
  $('#myCarousel .carousel-inner > .item > img').css('height', hei);

  $(window).resize(function(){
    var wid = $(window).width();
    var hei = wid / 2;
    $('#myCarousel').css('height', hei);
    $('#myCarousel .item').css('height', hei);
    $('#myCarousel .carousel-inner > .item > img').css('height', hei);
    if (wid < 1190) {
      $('.navbar-default').removeClass('navbar-fixed-top');
      $('#menu').css('margin-left', 0);
    } else if (wid > 1190) {
      $('#menu').css('margin-left', 94);
    }
  });

  // 判断carousel-indicators的个数
  var n = $('.carousel-inner .bigitem').length;
  for (var i = 0; i < n; i++) {
    var html = '<li data-slide-to="' + i + '" data-target="#myCarousel"></li>';
    $('ol.carousel-indicators').append(html);
  };

  // 设置slide默认当前项
  $('.carousel-inner .bigitem:eq(0)').addClass('active');
  $('.carousel-indicators li:eq(0)').addClass('active');
  if (n > 1) {
    $('#myCarousel').hover(
      function(){
        $('a.carousel-control').fadeIn();
      },
      function(){
        $('a.carousel-control').fadeOut();
      }
    );
  }

  // 全部及按导演筛选切换
  $('.getall').click(function() {
    $('#allList').fadeIn();
    $('#filmList1').empty();
  });
  $('.byDirector').click(function(){
    var category = $('#page_slug').attr('value');
    var director = $(this).attr('value');
    $('#allList').fadeOut();
    $('#filmList1').fadeOut();
    var url = './search/' + category + '/' + director;
    $('#filmList1').load(url).fadeIn();
  });

  // 推荐及按类型筛选切换
  $('.recommend').click(function(){
    $('#recommendList').fadeIn();
    $('#filmList2').empty();
  });
  $('.byType').click(function(){
    var type = $(this).attr('value');
    $('#recommendList').fadeOut();
    $('#filmList2').fadeOut();
    var url = './find/' + type;
    $('#filmList2').load(url).fadeIn();
  });
});