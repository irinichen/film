$(document).ready(function() {
	$('#content').redactor({
		buttons: ['bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'image', 'link']
	});
})