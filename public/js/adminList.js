require.config({
	baseUrl: '../js/',
	paths: {
		avalon: 'tools/avalon',
		domReady: 'tools/domReady'
	},
	shim: {
		avalon: { exports: 'avalon' }
	}
});

require(['avalon', "domReady!"], function() {

	// 分类信息 =================================================
	var category_m = avalon.define('category', function(vm) {
		vm.categories = {};

		// BEGIN OF REMOVE
		vm.remove = function(el) {

			var cid = el._id;

			$.ajax({
				type: 'delete',
				url: '/api/categories/' + cid
			})
			.done(function(result) {
				if (result.msg == 'success') {
					vm.categories.remove(el);
					alert('删除成功！');
				} else {
					alert(result.msg);
				};
			});

		};
		// END OF REMOVE
	});

	// 类型信息 =================================================
	var type_m = avalon.define('type', function(vm) {
		vm.types = {};

		// BEGIN OF REMOVE
		vm.remove = function(el) {

			var tid = el._id;
			var cid = el.category;

			$.ajax({
				type: 'delete',
				url: '/api/types/' + tid + '?category=' + cid
			})
			.done(function(result) {
				if (result.msg == 'success') {
					vm.types.remove(el);
					alert('删除成功！');
				} else {
					alert(result.msg);
				};
			});

		};
		// END OF REMOVE
	});

	$(function() {

		// 分类 ==================================================
		$.getJSON('/api/categories').done(function(data) {
			category_m.categories = data;
		});

		// 类型 ==================================================
		$.getJSON('/api/types?category=').done(function(data) {
			type_m.types = data;
		});

	});

	avalon.scan();
});