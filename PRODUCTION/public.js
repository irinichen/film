$(document).ready(function(){
  var wid = $(window).width();
  var hei = wid / 2;
  $('#myCarousel').css('height', hei);
  $('#myCarousel .item').css('height', hei);
  $('#myCarousel .carousel-inner > .item > img').css('height', hei);

  var n = $('.carousel-inner .bigitem').length;
  for (var i = 0; i < n; i++) {
    var html = '<li data-slide-to="' + i + '" data-target="#myCarousel"></li>';
    $('ol.carousel-indicators').append(html);
  };

  $('.carousel-inner .bigitem:eq(0)').addClass('active');
  $('.carousel-indicators li:eq(0)').addClass('active');
});