var Index 		= require('../app/controllers/index')
var User 		= require('../app/controllers/user')
var Movie 		= require('../app/controllers/movie')
var Category 	= require('../app/controllers/category')
var Type 		= require('../app/controllers/type')
var Director 	= require('../app/controllers/director')
var Article 	= require('../app/controllers/article')
var Slide 		= require('../app/controllers/slide')
var _ 			= require('underscore')

module.exports = function(app) {
	// PRE HANDLE USER
	app.use(function(req, res, next) {
		var _user = req.session.user

		app.locals.user = _user
		
		next()	
	})

	// PAGES
	app.get('/', Index.index)
	app.get('/about', Index.about)
	app.get('/translation', Index.translation)
	app.get('/admin', User.signin)
	app.get('/:page', Index.page)
	app.get('/all', Index.all)
	app.get('/article/:slug', Article.detail)

	app.get('/category', Category.show)
	app.get('/:director', Index.movies)

	// USER
	
	app.get('/admin/user', User.signinRequired, User.adminRequired, User.list)
	app.get('/admin/user/add', User.signinRequired, User.adminRequired, User.add)
	app.post('/admin/user/new', User.signinRequired, User.adminRequired, User.signup)
	app.post('/admin/login', User.login)
	app.get('/admin/logout', User.logout)

	app.get('/:page', Index.page)

	// MOVIE
	app.get('/movie/:id', Movie.detail)

	//	ADMIN PANEL MOVIE
	app.get('/admin/movie', User.signinRequired, User.adminRequired, Movie.list)
	app.get('/admin/movie/add', User.signinRequired, User.adminRequired, Movie.new)
	app.get('/admin/movie/update/:id', User.signinRequired, User.adminRequired, Movie.update)
	app.post('/admin/movie/new', User.signinRequired, User.adminRequired, Movie.savePoster, Movie.save)
	app.post('/admin/movie', User.signinRequired, User.adminRequired, Movie.del)
	app.get('/admin/movie/video/:id', User.signinRequired, User.adminRequired, Movie.uploadVideo)
	app.post('/admin/movie/video/save', User.signinRequired, User.adminRequired, Movie.saveVideo, Movie.updateByVideo)

	//	ADMIN PANEL CATEGORY
	app.get('/admin/category/add', User.signinRequired, User.adminRequired, Category.add)
	app.get('/admin/category', User.signinRequired, User.adminRequired, Category.list)
	app.post('/admin/category/save', User.signinRequired, User.adminRequired, Category.save)
	app.post('/admin/category', User.signinRequired, User.adminRequired, Category.del)

	//	ADMIN PANEL TYPE
	app.get('/admin/type/add', User.signinRequired, User.adminRequired, Type.add)
	app.get('/admin/type', User.signinRequired, User.adminRequired, Type.list)
	app.post('/admin/type/save', User.signinRequired, User.adminRequired, Type.save)
	app.post('/admin/type', User.signinRequired, User.adminRequired, Type.del)

	//	ADMIN PANEL DIRECTOR
	app.get('/admin/director/add', User.signinRequired, User.adminRequired, Director.add)
	app.get('/admin/director', User.signinRequired, User.adminRequired, Director.list)
	app.post('/admin/director/save', User.signinRequired, User.adminRequired, Director.save)
	app.post('/admin/director', User.signinRequired, User.adminRequired, Director.del)

	// ADMIN PANEL ARTICLE
	app.get('/admin/article', User.signinRequired, User.adminRequired, Article.list)
	app.get('/admin/article/add', User.signinRequired, User.adminRequired, Article.add)
	app.post('/admin/article/save', User.signinRequired, User.adminRequired, Article.save)

	app.get('/admin/slide', User.signinRequired, User.adminRequired, Slide.list)
	app.get('/admin/slide/add', User.signinRequired, User.adminRequired, Slide.add)
	app.get('/admin/slide/update/:id', User.signinRequired, User.adminRequired, Slide.update)
	app.post('/admin/slide/save', User.signinRequired, User.adminRequired, Slide.savePictures, Slide.save)
	app.post('/admin/slide', User.signinRequired, User.adminRequired, Slide.del)

}