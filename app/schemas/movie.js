var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ObjectId = Schema.Types.ObjectId

var MovieSchema = new Schema({
	category: {
		type: ObjectId,
		ref: 'Category'
	},
	type: {
		type: Array,
		ref: 'Type'
	},
	recommend: Number,
	title: String,
	title_en: String,
	summary: String,
	summary_en: String,
	length: Number,
	director: {
		type: Array,
		ref: 'Director'
	},
	cast: String,
	cast_en: String,
	publisher: String,
	publisher_en: String,
	poster: String,
	video: {
		mp4: String,
		webm: String
	},
	photo: Array,
	meta: {
		createAt: {
			type: Date,
			default: Date.now()
		},
		updateAt: {
			type: Date,
			default: Date.now()
		}
	}
})

MovieSchema.pre('save', function(next) {
	if (this.isNew) {
		this.meta.createAt = this.meta.updateAt = Date.now()
	}
	else {
		this.meta.updateAt = Date.now()
	}

	next()
})

MovieSchema.statics = {
	fetch: function(cb) {
		return this
			.find({})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	fetchForIndex: function(cb) {
		return this
			.find({})
			.sort({'meta.updateAt':-1})
			.limit(8)
			.exec(cb)
	},
	findById: function(id, cb) {
		return this
			.findOne({_id: id})
			.exec(cb)
	},
	findByCategory: function(category, cb) {
		return this
			.find({category: category})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	findByDirector: function(director, cb) {
		return this
			.find({director: director})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	findByRec: function(cb) {
		return this
			.find({recommend: 1})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	findByCnd: function(category, director, cb) {
		return this
			.find({category: category, director: director})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	findByType: function(type, cb) {
		return this
			.find({type: type})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	deletePhoto: function(id, photo, cb) {
		return this
			.update({_id: id}, { $pullAll: {photo: [photo] } })
			.exec(cb)
	}
}

module.exports = MovieSchema