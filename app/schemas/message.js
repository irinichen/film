var mongoose = require('mongoose')
var Schema = mongoose.Schema

var MessageSchema = new Schema({
	name: String,
	email: String,
	content: String,
	meta: {
		createAt: {
			type: Date,
			default: Date.now()
		}
	}
})

MessageSchema.pre('save', function(next) {
	this.meta.createAt = Date.now()

	next()
})

MessageSchema.statics = {
	fetch: function(cb) {
		return this
			.find({})
			.sort('meta.updateAt')
			.exec(cb)
	},
	findById: function(id, cb) {
		return this
			.findOne({_id: id})
			.exec(cb)
	}
}

module.exports = MessageSchema