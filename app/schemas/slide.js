var mongoose = require('mongoose')

var SlideSchema = new mongoose.Schema({
	page: String,
	layout: String,
	title: String,
	title_en: String,
	link: String,
	content: {
		bg: String,
		cn: String,
		en: String
	},
	meta: {
		createAt: {
			type: Date,
			default: Date.now()
		},
		updateAt: {
			type: Date,
			default: Date.now()
		}
	}
})

SlideSchema.pre('save', function(next) {
	if (this.isNew) {
		this.meta.createAt = this.meta.updateAt = Date.now()
	}
	else {
		this.meta.updateAt = Date.now()
	}

	next()
})

SlideSchema.statics = {
	fetch: function(cb) {
		return this
			.find({})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	},
	findById: function(id, cb) {
		return this
			.findOne({_id: id})
			.exec(cb)
	},
	findByPage: function(page, cb) {
		return this
			.find({page: page})
			.sort({'meta.updateAt':-1})
			.exec(cb)
	}
}

module.exports = SlideSchema