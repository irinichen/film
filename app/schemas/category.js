var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ObjectId = Schema.Types.ObjectId

var CategorySchema = new Schema({
	name: String,
	name_en: String,
	slug: String,
	types: [{
		type: ObjectId,
		ref: 'Type'
	}],
	movies: [{
		type: ObjectId,
		ref: 'Movie'
	}],
	meta: {
		createAt: {
			type: Date,
			default: Date.now()
		},
		updateAt: {
			type: Date,
			default: Date.now()
		}
	}
})

CategorySchema.pre('save', function(next) {
	if (this.isNew) {
		this.meta.createAt = this.meta.updateAt = Date.now()
	}
	else {
		this.meta.updateAt = Date.now()
	}

	next()
})

CategorySchema.statics = {
	fetch: function(cb) {
		return this
			.find({})
			.sort('meta.updateAt')
			.exec(cb)
	},
	findById: function(id, cb) {
		return this
			.findOne({_id: id})
			.exec(cb)
	},
	findBySlug: function(slug, cb) {
		return this
			.findOne({slug: slug})
			.exec(cb)
	},
	deleteType: function(id, types, cb) {
		return this
			.update({_id: id}, { $pullAll: {types: [types] } })
			.exec(cb)
	}
}

module.exports = CategorySchema