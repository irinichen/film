var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ArticleSchema = new Schema({
	lang: String,
	year: Number,
	title: String,
	content: String,
	meta: {
		createAt: {
			type: Date,
			default: Date.now()
		},
		updateAt: {
			type: Date,
			default: Date.now()
		}
	}
})

ArticleSchema.pre('save', function(next) {
	if (this.isNew) {
		this.meta.createAt = this.meta.updateAt = Date.now()
	}
	else {
		this.meta.updateAt = Date.now()
	}

	next()
})

ArticleSchema.statics = {
	fetch: function(cb) {
		return this
			.find({})
			.sort({'meta.updateAt': -1})
			.exec(cb)
	},
	fetchForCnIndex: function(cb) {
		return this
			.find({lang: 'cn'})
			.sort({'meta.updateAt': -1})
			.limit(4)
			.exec(cb)
	},
	fetchForEnIndex: function(cb) {
		return this
			.find({lang: 'en'})
			.sort({'meta.updateAt': -1})
			.limit(4)
			.exec(cb)
	},
	findById: function(id, cb) {
		return this
			.findOne({_id: id})
			.exec(cb)
	},
	findByCn: function(cb) {
		return this
			.find({lang: 'cn'})
			.sort({'meta.updateAt': -1})
			.exec(cb)
	},
	findByEn: function(cb) {
		return this
			.find({lang: 'en'})
			.sort({'meta.updateAt': -1})
			.exec(cb)
	}
}

module.exports = ArticleSchema