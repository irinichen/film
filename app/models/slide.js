var mongoose = require('mongoose')
var SlideSchema = require('../schemas/slide')
var Slide = mongoose.model('Slide', SlideSchema)

module.exports = Slide