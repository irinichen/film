var mongoose = require('mongoose')
var DirectorSchema = require('../schemas/director')
var Director = mongoose.model('Director', DirectorSchema)

module.exports = Director