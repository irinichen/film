var Movie = require('../models/movie')
var Category = require('../models/category')
var Type = require('../models/type')
var Director = require('../models/director')
var _ = require('underscore')
var fs = require('fs')
var path = require('path')


// DETAIL PAGE
exports.detail = function(req, res) {
	var id = req.params.id
	Movie.findById(id, function(err, movie) {
		var movie = movie
		var directorIdArray = movie.director
		
		var directorNameArray = []
		
		var count = 0
		
		for (var i = directorIdArray.length - 1; i >= 0; i--) {
			Director.findById(directorIdArray[i], function(err, director) {
				
				directorNameArray.unshift(director.name);

				
				count++;

				
				if (count === directorIdArray.length) {
					if (err) {
						console.log(err)
					}
					res.render('detail', {
						page_title: '影片《' + movie.title + '》详情页',
						movie: movie,
						directorNames: directorNameArray
					})
				}
			}) 
		}
					
	})
}

exports.detailEn = function(req, res) {
	var id = req.params.id
	Movie.findById(id, function(err, movie) {
		var movie = movie
		var directorIdArray = movie.director
		
		var directorNameArray = []
		
		var count = 0
		
		for (var i = directorIdArray.length - 1; i >= 0; i--) {
			Director.findById(directorIdArray[i], function(err, director) {
				
				directorNameArray.unshift(director.name_en);

				
				count++;

				
				if (count === directorIdArray.length) {
					if (err) {
						console.log(err)
					}
					res.render('en/detail', {
						page_title: movie.title_en,
						movie: movie,
						directorNames: directorNameArray
					})
				}
			}) 
		}
					
	})
}

//	ADMIN PANEL MOVIE LIST
exports.list = function(req, res) {

	var page = 0
	var count = 10
	var index = page * count

	Movie.fetch(function(err, movies) {
		if (err) {
			console.log(err)
		}
		var results = movies.slice(index, index + count)
		res.render('admin_movie', {
			page_title: '影片列表',
			page_slug: 'movie',
			currentPage: (page + 1),
			totalPage: Math.ceil(movies.length / count),
			movies: results
		})
	})
		
}

// Search or List with navigation
exports.search = function(req, res) {
	var page = parseInt(req.params.page, 10) - 1
	var count = 10
	var index = page * count

	Movie.fetch(function(err, movies) {
		if (err) {
			console.log(err)
		}
		var results = movies.slice(index, index + count)
		res.render('admin_movie_table_list', {
			currentPage: (page + 1),
			totalPage: Math.ceil(movies.length / count),
			movies: results
		})
	})
}

// ADMIN ADD MOVIE
exports.new = function(req, res) {
	Category.find({}, function(err, categories) {
		Type.find({}, function(err, types) {
			Director.find({}, function(err, directories) {
				res.render('admin_movie_form', {
					page_title: '添加影片',
					page_slug: 'movie',
					categories: categories,
					types: types,
					directories: directories,
					movie: {}
				})
			})
				
		})
	})
		
		
}

// ADMIN UPDATE MOVIE
exports.update = function(req, res) {
	var id = req.params.id

	if (id) {
		Movie.findById(id, function(err, movie) {
			Category.find({}, function(err, categories) {
				Type.find({}, function(err, types) {
					Director.find({}, function(err, directories) {
						res.render('admin_movie_form', {
							page_title: '添加影片',
							page_slug: 'movie',
							categories: categories,
							types: types,
							directories: directories,
							movie: movie
						})
					})
						
				})
			})
		})
	}
}

// ADMIN POSTER
exports.savePoster = function(req, res, next) {
	var posterData = req.files.uploadPoster
	var filePath = posterData.path
	var originalFilename = posterData.originalFilename

	if (originalFilename) {
		fs.readFile(filePath, function(err, data) {
			var timestamp = Date.now()
			var type = posterData.type.split('/')[1]
			var poster = timestamp + '.' + type
			var newPath = path.join(__dirname, '../../', '/public/uploads/img/' + poster)

			fs.writeFile(newPath, data, function(err) {
				req.poster = poster
				next()
			})
		})
	} else {
		next()
	}
}

// ADMIN POST MOVIE
exports.save = function(req, res) {
	var id = req.body.movie._id
	var movieObj = req.body.movie
	
	var _movie

	if (req.poster) {
		movieObj.poster = req.poster
	}
	console.log('let me see: ' + movieObj)

	if (id) {
		Movie.findById(id, function(err, movie) {
			if (err) {
				console.log(err)
			}
			console.log(req.body.movie)
			_movie = _.extend(movie, movieObj)
			_movie.save(function(err, movie) {
				if (err) {
					console.log(err)
				}

				res.redirect('/admin/movie')
			})
		})
	}
	else {
		_movie = new Movie(movieObj)

		var categoryId = _movie.category
		// var typeId = _movie.type
		// var directorId = _movie.director

		_movie.save(function(err, movie) {
			if (err) {
				console.log(err)
			}

			Category.findById(categoryId, function(err, category) {
				category.movies.push(movie._id)
				category.save(function(err, category) {
					res.redirect('/admin/movie')
				})
			})					
		})
	}
}

// ADMIN UPLOAD MOVIE
exports.uploadVideo = function(req, res) {
	var id = req.params.id

	if (id) {
		Movie.findById(id, function(err, movie) {
			res.render('admin_movie_video', {
				page_title: '上传片花',
				page_slug: 'movie',
				movie: movie
			})
		})
			
	}
}

// ADMIN VIDEO
exports.saveVideo = function(req, res, next) {
	var videoMp4Data = req.files.uploadVideoMp4
	var videoWebmData = req.files.uploadVideoWebm
	var mp4FilePath = videoMp4Data.path
	var webmFilePath = videoWebmData.path
	var mp4OriginalFilename = videoMp4Data.originalFilename
	var webmOriginalFilename = videoWebmData.originalFilename
	
	if (mp4OriginalFilename) {
		fs.readFile(mp4FilePath, function(err, data) {
			var timestamp = Date.now()
			var type = videoMp4Data.type.split('/')[1]
			var videoMp4 = timestamp + '.' + type
			var mp4NewPath = path.join(__dirname, '../../', '/public/uploads/video/' + videoMp4)

			fs.writeFile(mp4NewPath, data, function(err) {
				req.videoMp4 = videoMp4
				if (webmOriginalFilename) {
					fs.readFile(webmFilePath, function(err, data) {
						var timestamp = Date.now()
						var type = videoWebmData.type.split('/')[1]
						var videoWebm = timestamp + '.' + type
						var webmNewPath = path.join(__dirname, '../../', '/public/uploads/video/' + videoWebm)

						fs.writeFile(webmNewPath, data, function(err) {
							req.videoWebm = videoWebm
							next()
						})
					})
				} else {
					next()
				}
			})
		})
	} else {
		next()
	}

				
}

// ADMIN UPLOAD MOVIE VIDEO
exports.updateByVideo = function(req, res) {
	var id = req.body.movie._id
	var movieObj = req.body.movie
	
	var _movie

	if (req.videoMp4) {
		movieObj.video.mp4 = req.videoMp4
	}
	if (req.videoWebm) {
		movieObj.video.webm = req.videoWebm
	}

	Movie.findById(id, function(err, movie) {
		if (err) {
			console.log(err)
		}
		console.log('here or not here: ' + movie)
		_movie = _.extend(movie, movieObj)
		_movie.save(function(err, movie) {
			if (err) {
				console.log(err)
			}

			res.redirect('/admin/movie')
		})
	})

}

exports.loadType = function(req, res) {
	var category = req.params.category
	Type.findByCategory(category, function(err, types) {
		res.render('admin_type_list', {
			types: types
		})
	})
}

// 上传剧照的页面
exports.uploadPhoto = function(req, res) {
	var id = req.params.id
	if (id) {
		Movie.findById(id, function(err, movie) {
			res.render('admin_movie_gallery', {
				page_title: '上传剧照',
				page_slug: 'movie',
				movie: movie
			})
		})
	}
}

// 先把图片保存到目标文件夹，下一步在存入数据库
exports.savePhoto = function(req, res) {
	var id = req.body.movie._id
	var _movie
	var photoData = req.files.uploadPhoto
	var photoDataArray = []
	for (var i = 0; i < photoData.length; i++) {
		if (photoData[i].name) {
			photoDataArray.unshift(photoData[i])
		}
	}
	var length = photoDataArray.length
	if (length > 0) {
		Movie.findById(id, function(err, movie) {
			for (var i = length - 1; i >= 0; i--) {
				var newNameArray = []
				var photoData = photoDataArray[i]
				var photoPath = photoData.path
				var photoOriginalFilename = photoData.originalFilename
				if (photoOriginalFilename) {
					fs.readFile(photoPath, function(err, data) {
						var timestamp = Date.now()
						var type = photoData.type.split('/')[1]
						var photoNewName = timestamp + '.' + type
						var photoNewPath = path.join(__dirname, '../../', '/public/uploads/img/' + photoNewName)
						fs.writeFile(photoNewPath, data, function(err) {
							newNameArray.unshift(photoNewName)
							// console.log('保存成功，进入下一步。再看下newNameArray：' + newNameArray)
							fs.unlink(photoPath, function() {
								if (err) {
									console.log(err)
								}
								// else {
								// 	console.log('临时文件也被删了。')
								// }
							})
							if (newNameArray.length === length) {
								movie.photo = movie.photo.concat(newNameArray)
								_movie = _.extend(movie, movie)
							    _movie.save(function(err, movie) {
							    	if (err) {
										console.log(err)
									}
									// console.log('保存成功，让我们看下全新的movie：' + movie)
									res.redirect('/admin/movie')
							    })
							}
						})
					})
					
				} else {
					console.log('没有photoOriginalFilename')
				}
			}
		})
	} else {
		console.log('没有文件')
		res.redirect('/admin/movie/gallery/' + id)
	}
}

// ADMIN DELETE MOVIE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Movie.findById(id, function(err, movie) {
			var m = {}
			var m = movie
			var posterName = m.poster
			var mp4Name = m.video.mp4
			var webmName = m.video.webm
			if (posterName) {
				var posterPath = path.join(__dirname, '../../../', '/client/public/uploads/poster/' + posterName)
				fs.unlink(posterPath, function(err) {
					if (err) {
						console.log(err)
					}
					console.log('The movie poster file has been successfully deleted.')
				})
			}
			if (mp4Name) {
				var mp4Path = path.join(__dirname, '../../../', '/client/public/uploads/video/' + mp4Name)
				fs.unlink(mp4Path, function(err) {
					if (err) {
						console.log(err)
					}
					console.log('The movie mp4 video has been successfully deleted.')
				})
			}
			if (webmName) {
				var webmPath = path.join(__dirname, '../../../', '/client/public/uploads/video/' + webmName)
				fs.unlink(webmPath, function(err) {
					if (err) {
						console.log(err)
					}
					console.log('The movie webm video has been successfully deleted.')
				})
			}
				
		})
		Movie.remove({ _id: id }, function(err, movie) {
			if (err) {
				console.log(err)
				res.redirect('/admin/404')
			} else {
				res.json({ success: 1 })
			}
		})
	}
}

// 删除图片
exports.delPhoto = function(req, res) {
	var id = req.query.id
	var photo = req.query.name
	if (id) {
		var fileName = photo
		var filePath = path.join(__dirname, '../../', '/public/uploads/img/' + fileName)
		fs.unlink(filePath, function(err) {
			if (err) {
				console.log(err)
				res.redirect('/admin/404')
			}
			console.log('The movie photo file has been successfully deleted.')
			Movie.deletePhoto(id, photo, function(err, movie) {
				if (err) {
					console.log(err)
				}
				res.json({ success: 1 })
				console.log('the movie now is: ' + movie)
			})
			
		})
	}
}