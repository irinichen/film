var Slide = require('../models/slide')
var fs = require('fs')
var path = require('path')

// 可以实现用对象里面的新字段，替换掉对应的老字段
var _ = require('underscore')

exports.list = function(req, res) {

	Slide.fetch(function(err, slides) {
		if (err) {
			console.log(err)
		}
		res.render('admin_slide', {
			page_title: '幻灯列表',
			page_slug: 'slide',
			slides: slides
		})
	})
}

exports.add = function(req, res) {

	res.render('admin_slide_form', {
		page_title: '添加幻灯',
		page_slug: 'slide',
		slide: {}
	})
}

exports.update = function(req, res) {
	var id = req.params.id
	if (id) {
		Slide.findById(id, function(err, slide) {
			res.render('admin_slide_form', {
				page_title: '修改幻灯',
				page_slug: 'slide',
				slide: slide
			})
		})
	}
}

exports.savePictures = function(req, res, next) {
	var uploadBgData = req.files.uploadBg
	var uploadCnData = req.files.uploadCn
	var uploadEnData = req.files.uploadEn
	var bgFilePath = uploadBgData.path
	var cnFilePath = uploadCnData.path
	var enFilePath = uploadEnData.path
	var bgOriginalFilename = uploadBgData.originalFilename
	var cnOriginalFilename = uploadCnData.originalFilename
	var enOriginalFilename = uploadEnData.originalFilename

	if (bgOriginalFilename) {
		fs.readFile(bgFilePath, function(err, data) {
			var timestamp = Date.now()
			var type = uploadBgData.type.split('/')[1]
			var bg = timestamp + '.' + type
			var bgNewPath = path.join(__dirname, '../../', '/public/uploads/img/' + bg)

			fs.writeFile(bgNewPath, data, function(err) {
				req.bg = bg

				if (cnOriginalFilename) {
					fs.readFile(cnFilePath, function(err, data) {
						var timestamp = Date.now()
						var type = uploadCnData.type.split('/')[1]
						var cn = timestamp + '.' + type
						var cnNewPath = path.join(__dirname, '../../', '/public/uploads/img/' + cn)

						fs.writeFile(cnNewPath, data, function(err) {
							req.cn = cn

							if (enOriginalFilename) {
								fs.readFile(enFilePath, function(err, data) {
									var timestamp = Date.now()
									var type = uploadEnData.type.split('/')[1]
									var en = timestamp + '.' + type
									var enNewPath = path.join(__dirname, '../../', '/public/uploads/img/' + en)

									fs.writeFile(enNewPath, data, function(err) {
										req.en = en
										next()
									})
								})
							} else {
								next()
							}
						})
					})
				} else {
					next()
				}
			})
		})
	} else {
		next()
	}
}

exports.save = function(req, res) {
	var id = req.body.slide._id
	var slideObj = req.body.slide

	if (req.bg) {
		slideObj.content.bg = req.bg
	}
	if (req.cn) {
		slideObj.content.cn = req.cn
	}
	if (req.en) {
		slideObj.content.en = req.en
	}

	var _slide

	if (id) {
		Slide.findById(id, function(err, slide) {
			if (err) {
				console.log(err)
			}
			// 新字段替换掉老字段
			_slide = _.extend(slide, slideObj)
			_slide.save(function(err, slide) {	// 第二个参数是save后的数据
				if (err) {
					console.log(err)
				}

				res.redirect('/admin/slide')
			})
		})
	} else {
		var slide = new Slide(slideObj)

		slide.save(function(err, slide) {
			if (err) {
				console.log(err)
			}

			res.redirect('/admin/slide')
		})
	}
}

// ADMIN DELETE MOVIE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Slide.remove({ _id: id }, function(err, slide) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}