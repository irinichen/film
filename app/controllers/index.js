var Category 	= require('../models/category')
var Movie 		= require('../models/movie')
var Type 		= require('../models/type')
var Slide 		= require('../models/slide')
var Director 	= require('../models/director')
var Message		= require('../models/message')
var Article		= require('../models/article')

// HOME PAGE

exports.index = function(req, res) {
	Movie.fetchForIndex(function(err, movies) {
		if (err) {
			console.log(err)
		}
		Slide.findByPage('index', function(err, slides) {
			if (err) {
				console.log(err)
			}
			Article.fetchForCnIndex(function(err, articles) {
				var start_ptn = /<\/?[^>]*>/g
				var end_ptn = /[ | ]*\n/g
				var space_ptn = /&nbsp;/ig
				for (var i = articles.length - 1; i >= 0; i--) {
					var content = articles[i].content
					var content = content.replace(start_ptn,"").replace(end_ptn).replace(space_ptn,"")
					var content = content.substr(1, 50)
					articles[i].content = content
				}
					
					
				res.render('index', {
					page_title: '首页',
					page_slug: 'home',
					slides: slides,
					movies: movies,
					articles: articles
				})
			})
		})
	})
}

exports.indexEn = function(req, res) {

	Movie.fetchForIndex(function(err, movies) {
		if (err) {
			console.log(err)
		}
		Slide.findByPage('index', function(err, slides) {
			if (err) {
				console.log(err)
			}
			Article.fetchForCnIndex(function(err, articles) {
				var start_ptn = /<\/?[^>]*>/g
				var end_ptn = /[ | ]*\n/g
				var space_ptn = /&nbsp;/ig
				for (var i = articles.length - 1; i >= 0; i--) {
					var content = articles[i].content
					var content = content.replace(start_ptn,"").replace(end_ptn).replace(space_ptn,"")
					var content = content.substr(1, 50)
					articles[i].content = content
				}
				res.render('en/index', {
					page_title: '首页',
					page_title_en: 'Home',
					page_slug: 'home',
					slides: slides,
					movies: movies,
					articles: articles
				})
			})
				
		})
			
	})
		
}


// 前台分类页面
exports.page = function(req, res) {
	var page = req.params.page

	if (page) {
		Slide.findByPage(page, function(err, slides) {
			if (err) {
				console.log(err)
			}
			Category.findBySlug(page, function(err, category) {
				if (err) {
					console.log(err)
				}
				var category = new Category(category)

				Director.findByCategory(category._id, function(err, directors) {
					if (err) {
						console.log(err)
					}

					Type.findByCategory(category._id, function(err, types) {
						if (err) {
							console.log(err)
						}
						Movie.findByCategory(category._id, function(err, movies) {
							if (err) {
								console.log(err)
							}
							Movie.findByRec(function(err, recommends) {
								if (err) {
									console.log(err)
								}
								res.render('category', {
									page_slug: page,
									category: category,
									slides: slides,
									directors: directors,
									types: types,
									page_title: category.name,
									movies: movies,
									recommends: recommends
								})
							})
						})
					})
				})
			})
				
		})
	}
}

exports.pageEn = function(req, res) {
	var page = req.params.page

	if (page) {
		Slide.findByPage(page, function(err, slides) {
			if (err) {
				console.log(err)
			}
			Category.findBySlug(page, function(err, category) {
				if (err) {
					console.log(err)
				}
				var category = new Category(category)

				Director.findByCategory(category._id, function(err, directors) {
					if (err) {
						console.log(err)
					}

					Type.findByCategory(category._id, function(err, types) {
						if (err) {
							console.log(err)
						}
						Movie.findByCategory(category._id, function(err, movies) {
							if (err) {
								console.log(err)
							}
							Movie.findByRec(function(err, recommends) {
								if (err) {
									console.log(err)
								}
								res.render('en/category', {
									page_slug: page,
									category: category,
									slides: slides,
									directors: directors,
									types: types,
									page_title: category.name_en,
									movies: movies,
									recommends: recommends
								})
							})
						})
					})
				})
			})
				
		})
	}
}

exports.director = function(req, res) {
	var director = req.params.director

	if (director) {
		Movie.findByDirector(director, function(err, movies) {
			if (err) {
				console.log(err)
			}
			res.render('category_list', {
				movies: movies
			})
		})
	}
}

exports.movies = function(req, res) {
	var director = req.params.director
	if (director) {
		Movie.findByDirector(director._id, function(err, movies) {
			if (err) {
				console.log(err)
			}
			res.render('show_movies', {
				movies: movies
			})
		})
	}
}

exports.all = function(req, res) {

	Type
		.find({})
		.populate({path: 'movies', options: {limit: 5}})
		.exec(function(err, types) {
			if (err) {
				console.log(err)
			}
			res.render('all', {
				page_title: '测试',
				types: types
			})
		})
		
}

exports.about = function(req, res) {

	res.render('about', {
		page_title: '关于我们',
		page_slug: 'about'
	})
		
}

exports.aboutEn = function(req, res) {

	res.render('en/about', {
		page_title_en: 'About Us',
		page_slug: 'about'
	})
		
}

exports.translation = function(req, res) {

	res.render('translation', {
		page_title: '影视译制',
		page_slug: 'translation'
	})
		
}

exports.translationEn = function(req, res) {

	res.render('en/translation', {
		page_title_en: 'Film Translation',
		page_slug: 'translation'
	})
		
}

// exports.film = function(req, res) {

// 	res.render('category', {
// 		page_title: '故事片',
// 		page_slug: 'film'
// 	})
		
// }

// exports.documentary = function(req, res) {

// 	res.render('category', {
// 		page_title: '纪录片',
// 		page_slug: 'documentary'
// 	})
		
// }

// exports.animation = function(req, res) {

// 	res.render('category', {
// 		page_title: '美术片',
// 		page_slug: 'animation'
// 	})
		
// }

// exports.tv = function(req, res) {

// 	res.render('category', {
// 		page_title: '电视剧',
// 		page_slug: 'tv'
// 	})
		
// }

// exports.short = function(req, res) {

// 	res.render('category', {
// 		page_title: '短片',
// 		page_slug: 'short'
// 	})
		
// }