var Director = require('../models/director')
var Category = require('../models/category')
var _ = require('underscore')

//	ADMIN PANEL TYPE LIST
exports.list = function(req, res) {

	console.log('user in session: ')
	console.log(req.session.user)

	Director.fetch(function(err, directories) {
		if (err) {
			console.log(err)
		}
		res.render('admin_director', {
			page_title: '导演列表',
			page_slug: 'director',
			directories: directories
		})
	})
		
}

// ADMIN ADD TYPE
exports.add = function(req, res) {
	Category.find({}, function(err, categories) {
		res.render('admin_director_form', {
			page_title: '添加导演',
			page_slug: 'director',
			categories: categories,
			director: {}
		})
	})
		
}

// Open the director-update form page
exports.update = function(req, res) {
	var id = req.params.id
	var categories = []
	if (id) {
		Director.findById(id, function(err, director) {
			if (err) {
				console.log(err)
				res.redirect('/admin/404')
			}
			var categories = director.categories
			var categoryNameArray = []
			var l = categories.length
			for (var i = 0; i < l; i++) {
				// categoryArray.unshift(categories[i])
				var cateId = categories[i]
				Category.findById(cateId, function(err, category) {
					var cateName = category.name
					categoryNameArray.unshift(cateName)
					if (categoryNameArray.length === l) {
						res.render('admin_director_form', {
							page_title: '查看导演信息',
							page_slug: 'director',
							director: director,
							categories: categoryNameArray
						})
					}
				})
			}
			
						
		})
	} else {
		res.redirect('/admin/404')
	}
}

// ADMIN POST TYPE
exports.save = function(req, res) {
	var _director = req.body.director
	var director = new Director(_director)

	director.save(function(err, director) {
		if (err) {
			console.log(err)
		}

		res.redirect('/admin/director')
	})
}

// ADMIN DELETE MOVIE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Director.remove({ _id: id }, function(err, director) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}