var User = require('../models/user')

// LOGIN PAGE
exports.signin = function(req, res) {
	console.log('user in session: ')
	console.log(req.session.user)
	res.render('admin_login', {
		page_title: '登录'
	})
}

// ADMIN USER LIST
exports.list = function(req, res) {

	User.fetch(function(err, users) {
		if (err) {
			console.log(err)
		}
		res.render('admin_user', {
			page_title: '用户列表',
			page_slug: 'user',
			users: users
		})
	})
}

// ADMIN USER SIGNUP PAGE
exports.add = function(req, res) {
	res.render('admin_user_form', {
		page_title: '添加用户',
		page_slug: 'user'
	})
}

// ADMIN USER SIGNUP
exports.signup = function(req, res) {
	var _user = req.body.user
	

	User.find({name: _user.name}, function(err, user) {
		if (err) {
			console.log(err)
		}

		if (user) {
			return res.redirect('/admin')
		}
		else {
			var user = new User(_user)

			user.save(function(err, user) {
				if (err) {
					console.log(err)
				}

				res.redirect('/admin/user')
			})
		}
	})
			
}

// LOGIN
exports.login =  function(req, res) {
	var _user = req.body.user
	var name = _user.name
	var password = _user.password

	User.findOne({name: name}, function(err, user) {
		if (err) {
			console.log(err)
		}

		if (!user) {
			return redirect('/admin')
		}

		user.comparePassword(password, function(err, isMatch) {
			if (err) {
				console.log(err)
			}

			if (isMatch) {
				req.session.user = user
				return res.redirect('/admin/movie')
			}
			else {
				return res.redirect('/admin')
			}
		})
	})
}

// LOGOUT
exports.logout = function(req, res) {
	delete req.session.user
	// delete app.locals.user
	res.redirect('/')
}

// MIDWARE FOR USER
exports.signinRequired = function(req, res, next) {
	var user = req.session.user

	if (!user) {
		return res.redirect('/admin')
	}

	next()
}

exports.adminRequired = function(req, res, next) {
	var user = req.session.user

	if (user.role <= 10) {
		return res.redirect('/admin')
	}

	next()
}