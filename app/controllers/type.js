var Type = require('../models/type')
var Category = require('../models/category')
var _ = require('underscore')

//	ADMIN PANEL TYPE LIST
exports.list = function(req, res) {

	console.log('user in session: ')
	console.log(req.session.user)

	Type.fetch(function(err, types) {
		if (err) {
			console.log(err)
		}
		res.render('admin_type', {
			page_title: '类型列表',
			page_slug: 'type',
			types: types
		})
	})
		
}

// ADMIN ADD TYPE
exports.add = function(req, res) {
	Category.find({}, function(err, categories) {
		res.render('admin_type_form', {
			page_title: '添加类型',
			page_slug: 'type',
			categories: categories,
			type: {}
		})
			
	})
		
}

// ADMIN POST TYPE
exports.save = function(req, res) {
	var _type = req.body.type
	var type = new Type(_type)

	var categoryId = _type.category

	type.save(function(err, type) {
		if (err) {
			console.log(err)
		}

		Category.findById(categoryId, function(err, category) {
			category.types.push(type._id)

			category.save(function(err, category) {
				res.redirect('/admin/type')
			})
		})
				
	})
}

// ADMIN DELETE MOVIE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Type.remove({ _id: id }, function(err, movie) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}