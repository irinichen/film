var Article = require('../models/article')
var _ = require('underscore')

// DETAIL PAGE
exports.detail = function(req, res) {
	var id = req.params.id
	Article.findById(id, function(err, article) {
		res.render('article', {
			page_title: '文章页面',
			article: article
		})
	})
		
}

//	ADMIN PANEL ARTICLE LIST
exports.list = function(req, res) {

	console.log('user in session: ')
	console.log(req.session.user)

	Article.fetch(function(err, articles) {
		if (err) {
			console.log(err)
		}
		res.render('admin_article', {
			page_title: '文章列表',
			page_slug: 'article',
			articles: articles
		})
	})
		
}

// ADMIN ADD ARTICLE
exports.add = function(req, res) {
	res.render('admin_article_form', {
		page_title: '写文章',
		page_slug: 'article',
		article: {}
	})
}

// ADMIN SAVE ENGLISH ARTICLE


// ADMIN POST ARTICLE
exports.save = function(req, res) {
	var _article = req.body.article
	var article = new Article(_article)
	console.log('Can I see this? ' + article)

	article.save(function(err, article) {
		if (err) {
			console.log(err)
		}

		res.redirect('/admin/article')
	})
}

// ADMIN DELETE ARTICLE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Article.remove({ _id: id }, function(err, article) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}
