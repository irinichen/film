var Category = require('../models/category');
var Type = require('../models/type');

// CATEGORY Tasks =======================================
// Category whole list
exports.categories = function(req, res) {

	Category.fetch(function(err, categories) {
		if (err) {
			console.log(err);
		};
		res.json(categories);
	});

};

// Category single item
exports.category = function(req, res) {

	var id = req.params.id;

	if (id) {

		Category.findById(id, function(err, category) {
			if (err) {
				console.log(err);
			};
			res.json(category);
		});

	} else {
		res.json({ "message": "id不能为空。" });
	};

};

// Category item delete
exports.cateDelete = function(req, res) {

	var id = req.params.id;

	if (id) {
		Category.remove({_id: id}, function(err, category) {
			if (err) {
				console.log(err);
			} else {
				res.json({ msg: 'success' });
			};
		});
	} else {
		res.json({ msg: '没有这个分类项...' });
	};
};

// TYPE Tasks ===========================================
// Type whole list
exports.types = function(req, res) {

	var cid = req.param('category');

	if (cid) {

		Type.findByCategory(cid, function(err, types) {
			if (err) {
				console.log(err);
			};
			res.json(types);
		});

		// res.json({ 'category': cid });

	} else {
		Type.fetch(function(err, types) {
			if (err) {
				console.log(err);
			};
			res.json(types);
		});
	};

};

// Type single item
exports.type = function(req, res) {

	var id = req.params.id;

	if (id) {

		Type.findById(id, function(err, type) {
			if (err) {
				console.log(err);
			};
			res.json(type);
		});

	} else {
		res.json({ "message": "id不能为空。" });
	};

};

// Type item delete
exports.typeDelete = function(req, res) {

	var tid = req.params.id;
	var cid = req.query.category;

	if (tid) {

		Category.deleteType(cid, tid, function(err, category) {
			Type.remove({_id: tid}, function(err, post) {
				if (err) {
					console.log(err);
				} else {
					res.json({ msg: 'success' });
				};
			});
		});
			
	} else {
		res.json({ msg: '没有这篇文章...' });
	};
};