var Category 	= require('../models/category')
var Movie 		= require('../models/movie')
var Type 		= require('../models/type')
var Slide 		= require('../models/slide')
var Director 	= require('../models/director')
var Message		= require('../models/message')


exports.director = function(req, res) {
	var director = req.params.director

	if (director) {
		Movie.findByDirector(director, function(err, movies) {
			if (err) {
				console.log(err)
			}
			res.render('category_list', {
				movies: movies
			})
		})
	}
}

exports.cnd = function(req, res) {
	var category = req.params.category
	var director = req.params.director

	if (category) {
		if (director) {
			Movie.findByCnd(category, director, function(err, movies) {
				if (err) {
					console.log(err)
				}
				res.render('category_list', {
					movies: movies
				})
			})
		}
	}
}

exports.cndEn = function(req, res) {
	var category = req.params.category
	var director = req.params.director

	if (category) {
		if (director) {
			Movie.findByCnd(category, director, function(err, movies) {
				if (err) {
					console.log(err)
				}
				res.render('en/category_list', {
					movies: movies
				})
			})
		}
	}
}

exports.type = function(req, res) {
	var type = req.params.type
	if (type) {
		Movie.findByType(type, function(err, movies) {
			if (err) {
				console.log(err)
			}
			res.render('category_list2', {
				movies: movies
			})
		})
	}
}

exports.typeEn = function(req, res) {
	var type = req.params.type
	if (type) {
		Movie.findByType(type, function(err, movies) {
			if (err) {
				console.log(err)
			}
			res.render('en/category_list2', {
				movies: movies
			})
		})
	}
}
