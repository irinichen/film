var Movie = require('../models/movie')
var Category = require('../models/category')
var Type = require('../models/type')
var Director = require('../models/director')
var _ = require('underscore')
var fs = require('fs')
var path = require('path')

// DETAIL PAGE
exports.detail = function(req, res) {
	var id = req.params.id
	Movie.findById(id, function(err, movie) {
		var movie = movie
		var directorId = movie.director
		Director.findById(directorId, function(err, director) {
			if (err) {
				console.log(err)
			}
			var directorName = director.name
			res.render('detail', {
				page_title: '影片《' + movie.title + '》详情页',
				movie: movie,
				directorName: directorName
			})
		})
			
	})
		
}

//	ADMIN PANEL MOVIE LIST
exports.list = function(req, res) {

	console.log('user in session: ')
	console.log(req.session.user)

	Movie.fetch(function(err, movies) {
		if (err) {
			console.log(err)
		}
		res.render('admin_movie', {
			page_title: '影片列表',
			page_slug: 'movie',
			movies: movies
		})
	})
		
}

// ADMIN ADD MOVIE
exports.new = function(req, res) {
	Category.find({}, function(err, categories) {
		Type.find({}, function(err, types) {
			Director.find({}, function(err, directories) {
				res.render('admin_movie_form', {
					page_title: '添加影片',
					page_slug: 'movie',
					categories: categories,
					types: types,
					directories: directories,
					movie: {}
				})
			})
				
		})
	})
		
		
}

// ADMIN UPDATE MOVIE
exports.update = function(req, res) {
	var id = req.params.id

	if (id) {
		Movie.findById(id, function(err, movie) {
			Category.find({}, function(err, categories) {
				Type.find({}, function(err, types) {
					Director.find({}, function(err, directories) {
						res.render('admin_movie_form', {
							page_title: '添加影片',
							page_slug: 'movie',
							categories: categories,
							types: types,
							directories: directories,
							movie: movie
						})
					})
						
				})
			})
		})
	}
}

// ADMIN POSTER
exports.savePoster = function(req, res, next) {
	var posterData = req.files.uploadPoster
	var filePath = posterData.path
	var originalFilename = posterData.originalFilename

	if (originalFilename) {
		fs.readFile(filePath, function(err, data) {
			var timestamp = Date.now()
			var type = posterData.type.split('/')[1]
			var poster = timestamp + '.' + type
			var newPath = path.join(__dirname, '../../', '/public/uploads/img/' + poster)

			fs.writeFile(newPath, data, function(err) {
				req.poster = poster
				next()
			})
		})
	} else {
		next()
	}
}

// ADMIN POST MOVIE
exports.save = function(req, res) {
	var id = req.body.movie._id
	var movieObj = req.body.movie
	
	var _movie

	if (req.poster) {
		movieObj.poster = req.poster
	}
	console.log('let me see: ' + movieObj)

	if (id) {
		Movie.findById(id, function(err, movie) {
			if (err) {
				console.log(err)
			}
			console.log(req.body.movie)
			_movie = _.extend(movie, movieObj)
			_movie.save(function(err, movie) {
				if (err) {
					console.log(err)
				}

				res.redirect('/admin/movie')
			})
		})
	}
	else {
		_movie = new Movie(movieObj)

		var categoryId = _movie.category
		var typeId = _movie.type
		var directorId = _movie.director

		_movie.save(function(err, movie) {
			if (err) {
				console.log(err)
			}

			Category.findById(categoryId, function(err, category) {
				category.movies.push(movie._id)
				category.save(function(err, category) {
					Type.findById(typeId, function(err, type) {
						type.movies.push(movie._id)
						type.save(function(err, type) {
							Director.findById(directorId, function(err, director) {
								director.movies.push(movie._id)
								director.save(function(err, director) {
									res.redirect('/admin/movie')
								})
							})
						})
					})
				})
			})					
		})
	}
}

// ADMIN UPLOAD MOVIE
exports.uploadVideo = function(req, res) {
	var id = req.params.id

	if (id) {
		Movie.findById(id, function(err, movie) {
			res.render('admin_movie_video', {
				page_title: '上传片花',
				page_slug: 'movie',
				movie: movie
			})
		})
			
	}
}

// ADMIN VIDEO
exports.saveVideo = function(req, res, next) {
	var videoMp4Data = req.files.uploadVideoMp4
	var videoWebmData = req.files.uploadVideoWebm
	var mp4FilePath = videoMp4Data.path
	var webmFilePath = videoWebmData.path
	var mp4OriginalFilename = videoMp4Data.originalFilename
	var webmOriginalFilename = videoWebmData.originalFilename
	
	if (mp4OriginalFilename) {
		fs.readFile(mp4FilePath, function(err, data) {
			var timestamp = Date.now()
			var type = videoMp4Data.type.split('/')[1]
			var videoMp4 = timestamp + '.' + type
			var mp4NewPath = path.join(__dirname, '../../', '/public/uploads/video/' + videoMp4)

			fs.writeFile(mp4NewPath, data, function(err) {
				req.videoMp4 = videoMp4
				if (webmOriginalFilename) {
					fs.readFile(webmFilePath, function(err, data) {
						var timestamp = Date.now()
						var type = videoWebmData.type.split('/')[1]
						var videoWebm = timestamp + '.' + type
						var webmNewPath = path.join(__dirname, '../../', '/public/uploads/video/' + videoWebm)

						fs.writeFile(webmNewPath, data, function(err) {
							req.videoWebm = videoWebm
							next()
						})
					})
				} else {
					next()
				}
			})
		})
	} else {
		next()
	}

				
}

// ADMIN UPLOAD MOVIE VIDEO
exports.updateByVideo = function(req, res) {
	var id = req.body.movie._id
	var movieObj = req.body.movie
	
	var _movie

	if (req.videoMp4) {
		movieObj.video.mp4 = req.videoMp4
	}
	if (req.videoWebm) {
		movieObj.video.webm = req.videoWebm
	}

	Movie.findById(id, function(err, movie) {
		if (err) {
			console.log(err)
		}
		console.log('here or not here: ' + movie)
		_movie = _.extend(movie, movieObj)
		_movie.save(function(err, movie) {
			if (err) {
				console.log(err)
			}

			res.redirect('/admin/movie')
		})
	})

}

exports.loadType = function(req, res) {
	var category = req.params.category
	Type.findByCategory(category, function(err, types) {
		res.render('admin_type_list', {
			types: types
		})
	})
}

// ADMIN DELETE MOVIE
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Movie.remove({ _id: id }, function(err, movie) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}