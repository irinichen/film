var Category = require('../models/category')
var _ = require('underscore')

exports.show = function(req, res) {
	Category
		.find({})	// 找到所有的分类
		.populate({
			path: 'movies',
			// select: '',
			options: {
				limit: 5
			}
		})
		.exec(function(err, categories) {
			if (err) {
				console.log(err)
			}
			res.render('category_tmp', {
				page_title: '分类页面',
				categories: categories
			})
		})
}

//	ADMIN PANEL CATEGORY LIST
exports.list = function(req, res) {

	console.log('user in session: ')
	console.log(req.session.user)

	Category.fetch(function(err, categories) {
		if (err) {
			console.log(err)
		}
		res.render('admin_category', {
			page_title: '分类列表',
			page_slug: 'category',
			categories: categories
		})
	})
		
}

// ADMIN ADD CATEGORY
exports.add = function(req, res) {
	res.render('admin_category_form', {
		page_title: '添加分类',
		page_slug: 'category',
		category: {}
	})
}

// ADMIN POST CATEGORY
exports.save = function(req, res) {
	var id = req.body.category._id;
	var categoryObj = req.body.category;

	var _category;

	if (id) {
		Category.findById(id, function(err, category) {
			if (err) {
				console.log('sth wrong of find by categoryId: ' + err);
			};
			_category = _.extend(category, categoryObj);
			_category.save(function(err, category) {
				if (err) {
					console.log('sth wrong of update: ' + err);
				};
				res.redirect('/admin/category');
			});
		});
	} else {
		_category = new Category(categoryObj);
		_category.save(function(err, category) {
			if (err) {
				console.log('sth wrong of save data: ' + err);
			};
			res.redirect('/admin/category')
		});
	};
};

// ADMIN DELETE CATEGORY
exports.del = function(req, res) {
	var id = req.query.id
	if (id) {
		Category.remove({ _id: id }, function(err, category) {
			if (err) {
				console.log(err)
			}
			else {
				res.json({ success: 1 })
			}
		})
	}
}